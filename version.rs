// Copyright (c) 2020 Intel Corporation
//
// SPDX-License-Identifier: Apache-2.0
//

//
// WARNING: This file is auto-generated - DO NOT EDIT!
//

#![allow(dead_code)]

pub const AGENT_VERSION: &str = "3.2.0";
pub const API_VERSION: &str = "0.0.1";
pub const VERSION_COMMIT: &str = "3.2.0-15e85b8ce740faf026e7d72d0d950cd9e0bbfc1d";
pub const GIT_COMMIT: &str = "15e85b8ce740faf026e7d72d0d950cd9e0bbfc1d";
pub const AGENT_NAME: &str = "kata-agent";
pub const AGENT_DIR: &str = "/usr/bin";
pub const AGENT_PATH: &str = "/usr/bin/kata-agent";
